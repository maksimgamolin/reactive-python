from datetime import datetime
from threading import Thread
from random import randrange
from time import sleep
import socket


def extract():
    # Создаем сокет
    sock = socket.socket()
    # Просим у операционной системы выделить 9090 порт
    sock.bind(('', 9090))
    # Начинаем прослушку
    sock.listen(1)
    conn, addr = sock.accept()
    try:
        while True:
            raw_data = conn.recv(1024)
            if not raw_data:
                sleep(1)
                continue
            # уберем перевод строки
            data = raw_data[:-1].decode('utf-8')
            if data == 'stop':
                break
            yield data
    finally:
        conn.close()


class Observable:

    def __init__(self):
        self._subscribers = []
        self._map = []
        self._filters = []
        self._data_gen = extract()

    def subscribe(self, *args):
        self._subscribers += args

        for item in self._data_gen:
            # on_error
            if isinstance(item, Exception):
                for sub in self._subscribers:
                    Thread(target=sub.on_error, args=(item,)).start()
                continue
            # on_next
            if not all(f(item) for f in self._filters):
                continue

            new_item = item

            for m in self._map:
                new_item = m(new_item)

            for sub in self._subscribers:
                Thread(target=sub.on_next, args=(new_item,)).start()
        else:
            # on_complete
            for sub in self._subscribers:
                Thread(target=sub.on_complete).start()

    def map(self, *args):
        self._map += args
        return self

    def filter(self, *args):
        self._filters += args
        return self


def to_upper(item):
    return item.upper()


def min_len(item):
    return len(item) >= 5


class BaseObserver:

    def on_next(self, data):
        pass

    def on_error(self, data):
        pass

    def on_complete(self):
        pass


class PrintObserver(BaseObserver):

    def on_next(self, data):
        sleep(randrange(1, 5))
        print(data)

    def on_error(self, data):
        sleep(randrange(1, 5))
        print(f'Error: {data}')


class WriteFileObserver(BaseObserver):

    def on_next(self, data):
        with open('f.txt', 'a') as f:
            f.write(f'{data}\n')

    def on_complete(self):
        with open('f.txt', 'a') as f:
            f.write(f'End of File at {datetime.now().isoformat()}\n')


if __name__ == '__main__':
    Observable().filter(
        min_len
    ).map(
        to_upper
    ).subscribe(
        PrintObserver(), WriteFileObserver()
    )
